import { ChangeDetectionStrategy, Component } from '@angular/core';
import { SidebarToggleService } from '../../services/sidebar-toggle.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu-main',
  templateUrl: './menu-main.component.html',
  styleUrls: ['./menu-main.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MenuMainComponent {
  private list = this.router.config;

  constructor(
    private serviceToggle: SidebarToggleService,
    private router: Router,
  ) {
    console.log(this.list);
  }

  toggleMenu(): void {
    this.serviceToggle.toggle();
  }
}
