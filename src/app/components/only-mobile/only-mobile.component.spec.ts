import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OnlyMobileComponent } from './only-mobile.component';

describe('OnlyMobileComponent', () => {
  let component: OnlyMobileComponent;
  let fixture: ComponentFixture<OnlyMobileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OnlyMobileComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OnlyMobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
