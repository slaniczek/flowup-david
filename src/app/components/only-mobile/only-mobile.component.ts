import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { MediaQueriesService } from '../../services/media-queries.service';

@Component({
  selector: 'app-only-mobile',
  templateUrl: './only-mobile.component.html',
  styleUrls: ['./only-mobile.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OnlyMobileComponent {
  @Input('reverse') reverse = false;

  private isMobile$ = this.mediaQueries.maxWidth('sm');

  constructor(private mediaQueries: MediaQueriesService) {
    console.log(this.isMobile$);
  }
}
