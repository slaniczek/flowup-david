import { Component, ChangeDetectionStrategy, Input } from '@angular/core';
import { TableModel } from '../../models/table';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TableComponent {
  @Input('data') data: TableModel[] = [];

  getRowData(row: TableModel): any[] {
    return Object.keys(row).reduce((acc, item) => [...acc, row[item]], []);
  }

  getColumnsHeadings(): any[] {
    return Object.keys(this.data[0]);
  }

  constructor() {}
}
