import { ChangeDetectionStrategy, Component } from '@angular/core';
import { map } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-title',
  templateUrl: './title.component.html',
  styleUrls: ['./title.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TitleComponent {
  private routeTitle$ = this.route.data.pipe(map(({ name }) => name));
  private routeIcon$ = this.route.data.pipe(map(({ icon }) => icon));

  constructor(private route: ActivatedRoute) {
    console.log(this.routeTitle$, this.routeIcon$);
  }
}
