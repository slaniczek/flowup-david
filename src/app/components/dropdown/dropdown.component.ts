import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from '@angular/core';
import { DropdownModel } from '../../models/dropdown';

@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DropdownComponent implements OnInit {
  @Input('data') data: DropdownModel;

  private isOpen = false;

  toggle(): void {
    this.isOpen = !this.isOpen;
  }

  hide(): void {
    this.isOpen = false;
  }

  show(): void {
    this.isOpen = true;
  }

  constructor() {}

  ngOnInit() {
    console.log(this.data);
  }
}
