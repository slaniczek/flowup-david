import { Component, ChangeDetectionStrategy, Input } from '@angular/core';
import { DropdownItemModel } from '../../../../models/dropdown-item';

@Component({
  selector: 'app-default-item',
  templateUrl: './default-item.component.html',
  styleUrls: ['./default-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DefaultItemComponent {
  @Input('data') data: DropdownItemModel;

  constructor() {}
}
