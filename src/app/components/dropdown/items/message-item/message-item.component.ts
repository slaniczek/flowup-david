import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { DropdownItemModel } from '../../../../models/dropdown-item';

@Component({
  selector: 'app-message-item',
  templateUrl: './message-item.component.html',
  styleUrls: ['./message-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MessageItemComponent {
  @Input('data') data: DropdownItemModel;

  constructor() {}
}
