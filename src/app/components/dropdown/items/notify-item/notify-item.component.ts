import { Component, ChangeDetectionStrategy, Input } from '@angular/core';
import { DropdownItemModel } from '../../../../models/dropdown-item';

@Component({
  selector: 'app-notify-item',
  templateUrl: './notify-item.component.html',
  styleUrls: ['./notify-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NotifyItemComponent {
  @Input('data') data: DropdownItemModel;
  constructor() {
    console.log(this.data);
  }
}
