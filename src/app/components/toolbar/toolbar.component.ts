import { ChangeDetectionStrategy, Component } from '@angular/core';
import { DropdownModel } from '../../models/dropdown';
import { DropdownItemModel } from '../../models/dropdown-item';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ToolbarComponent {
  defaultItem: DropdownItemModel = {
    title: 'Link 1',
    date: new Date(),
    read: false,
    bold: false,
    author: 'David Slanina',
  };

  dataDropdown: DropdownModel = {
    icon: 'icofont-navigation-menu',
    type: 'default',
    notify: false,
    list: [this.defaultItem],
  };

  dataDropdownMessage: DropdownModel = {
    icon: 'icofont-ui-message',
    type: 'message',
    notify: true,
    list: [this.defaultItem],
  };

  dataDropdownNotify: DropdownModel = {
    icon: 'icofont-notification',
    type: 'notify',
    notify: true,
    list: [this.defaultItem],
  };

  constructor() {}
}
