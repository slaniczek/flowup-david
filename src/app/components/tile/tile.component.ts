import { Component, ChangeDetectionStrategy, Input } from '@angular/core';

@Component({
  selector: 'app-tile',
  templateUrl: './tile.component.html',
  styleUrls: ['./tile.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TileComponent {
  @Input('iconClassName') iconClassName: string;
  @Input('title') title: string;
  @Input('value') value: number;
  @Input('bottomText') bottomText: string;
  @Input('color') color: string;
  @Input('currency') currency = '$';

  constructor() {}
}
