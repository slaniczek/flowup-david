import { Component, ChangeDetectionStrategy } from '@angular/core';
import { SidebarToggleService } from '../../services/sidebar-toggle.service';

@Component({
  selector: 'app-toggler',
  templateUrl: './toggler.component.html',
  styleUrls: ['./toggler.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TogglerComponent {
  constructor(private serviceToggle: SidebarToggleService) {}

  toggle(): void {
    this.serviceToggle.toggle();
  }
}
