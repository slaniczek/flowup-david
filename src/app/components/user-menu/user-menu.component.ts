import { ChangeDetectionStrategy, Component } from '@angular/core';
import { DropdownUserModel } from '../../models/dropdown-user';
import { DropdownItemModel } from '../../models/dropdown-item';

@Component({
  selector: 'app-user-menu',
  templateUrl: './user-menu.component.html',
  styleUrls: ['./user-menu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserMenuComponent {
  private dataItem: DropdownItemModel = {
    title: 'Link',
    read: false,
    bold: false,
  };

  dataItem2: DropdownItemModel = {
    title: 'Log out',
    bold: true,
    read: false,
    color: 'red',
  };

  data: DropdownUserModel = {
    userName: 'David Slanina',
    list: [this.dataItem, this.dataItem2],
  };

  private isOpen: boolean = false;

  toggle(): void {
    this.isOpen = !this.isOpen;
  }

  hide(): void {
    this.isOpen = false;
  }

  show(): void {
    this.isOpen = true;
  }

  constructor() {}
}
