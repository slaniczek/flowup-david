import { Component, ChangeDetectionStrategy } from '@angular/core';
import { TileModel } from '../../models/tile';

@Component({
  selector: 'app-tile-list',
  templateUrl: './tile-list.component.html',
  styleUrls: ['./tile-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TileListComponent {
  private tile1: TileModel = {
    iconClassName: 'icofont-dashboard-web',
    title: 'Stock Total',
    bottomText: 'Increased by 60%',
    value: 120000,
    color: 'violet',
  };
  private tile2: TileModel = {
    iconClassName: 'icofont-database',
    title: 'Stock Total',
    bottomText: 'Increased by 40%',
    value: 15000,
    color: 'blue',
  };
  private tile3: TileModel = {
    iconClassName: 'icofont-flag',
    title: 'Stock Total',
    bottomText: 'Increased by 70%',
    value: 800,
    color: 'orange',
  };

  private list: TileModel[] = [this.tile1, this.tile2, this.tile3];

  constructor() {
    console.log(this.list);
  }
}
