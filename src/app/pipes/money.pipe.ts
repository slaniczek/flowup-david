import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'money',
})
export class MoneyPipe implements PipeTransform {
  // private currency = "Kč";

  stringToMoney(value: number, currency: string): string {
    switch (currency) {
      case '$':
        return currency + value;
      default:
        return `${value} ${currency}`;
    }
  }

  transform(value: number, args: string = '$'): string {
    return this.stringToMoney(value, args);
  }
}
