import { TestBed, inject } from '@angular/core/testing';

import { MediaQueriesService } from './media-queries.service';

describe('MediaQueriesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MediaQueriesService],
    });
  });

  it('should be created', inject(
    [MediaQueriesService],
    (service: MediaQueriesService) => {
      expect(service).toBeTruthy();
    },
  ));
});
