import { Injectable } from '@angular/core';
import { BreakpointObserver } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { BreakpointState } from '@angular/cdk/typings/layout';
import { MediaQueriesModel } from '../models/media-queries';

@Injectable({
  providedIn: 'root',
})
export class MediaQueriesService {
  private mediaQueries: MediaQueriesModel = {
    sm: {
      min: 576,
      max: 767,
    },
    md: {
      min: 768,
      max: 991,
    },
    lg: {
      min: 992,
      max: 1199,
    },
    xl: {
      min: 1200,
      max: 1349,
    },
    xxl: {
      min: 1350,
      max: 1599,
    },
    xxxl: {
      min: 1600,
      max: 999999,
    },
  };

  constructor(public breakpointObserver: BreakpointObserver) {}

  getMaxBreakpoint(key: string): number {
    return this.mediaQueries[key].max;
  }

  getMinBreakpoint(key: string): number {
    return this.mediaQueries[key].min;
  }

  public maxWidth = (key: string): Observable<BreakpointState> =>
    this.breakpointObserver.observe(
      `(max-width: ${this.getMaxBreakpoint(key)}px)`,
    );
  public minWidth = (key: string): Observable<BreakpointState> =>
    this.breakpointObserver.observe(
      `(min-width: ${this.getMinBreakpoint(key)}px)`,
    );
}
