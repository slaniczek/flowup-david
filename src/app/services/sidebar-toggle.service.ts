import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class SidebarToggleService {
  public isOpen$ = new BehaviorSubject<boolean>(false);

  constructor() {}

  toggle(): void {
    this.isOpen$.next(!this.isOpen$.value);
  }
}
