import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './views/home/home.component';
import { TypographyComponent } from './views/typography/typography.component';

const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent,
    data: {
      name: 'Dashboard',
      icon: 'icofont-home',
      menu: true,
    },
  },
  {
    path: 'typography',
    component: TypographyComponent,
    data: {
      name: 'Typography',
      icon: 'icofont-text-height',
      menu: true,
    },
  },
  {
    path: 'typography2',
    component: TypographyComponent,
    data: {
      name: 'Typography 2',
      icon: 'icofont-page',
      menu: true,
    },
  },
  { path: '**', redirectTo: '/home', data: { menu: false } },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
