export interface MenuItemModel {
  name: string;
  link: string;
  icon: string;
  active: boolean;
}
