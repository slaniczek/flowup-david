export interface MessageModel {
  id: string;
  title: string;
  author: string; // AuthorModel
  message: string;
  date: Date;
}
