export interface MediaQueriesModel {
  [name: string]: {
    min: number;
    max: number;
  };
}
