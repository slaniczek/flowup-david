export interface TileModel {
  iconClassName: string;
  title: string;
  bottomText?: string;
  value: number;
  color: string;
}
