export interface DropdownItemModel {
  title: string;
  date?: Date;
  read: boolean;
  bold: boolean;
  color?: string;
  author?: string;
}
