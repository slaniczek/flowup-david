import { DropdownItemModel } from './dropdown-item';

export interface DropdownUserModel {
  userName: string;
  photoLink?: string;
  list: DropdownItemModel[];
}
