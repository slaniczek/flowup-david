import { DropdownItemModel } from './dropdown-item';

export interface DropdownModel {
  icon: string;
  type: string;
  notify: boolean;
  list: DropdownItemModel[];
}
