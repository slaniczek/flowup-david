import {
  Directive,
  ElementRef,
  HostListener,
  Output,
  EventEmitter,
} from '@angular/core';

@Directive({
  selector: '[clickOutside]',
})
export class ClickOutsideDirective {
  constructor(private _elementRef: ElementRef) {}

  @Output('clickOutside') clickOutside: EventEmitter<void> = new EventEmitter();

  @HostListener('document:click', ['$event.target']) onMouseEnter(
    targetElement: ElementRef,
  ) {
    const clickedInside = this._elementRef.nativeElement.contains(
      targetElement,
    );
    if (!clickedInside) {
      this.clickOutside.emit();
    }
  }
}
