import { Component, ChangeDetectionStrategy } from '@angular/core';
import { PersonModel } from '../../models/person';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeComponent {
  private tableData: PersonModel[] = [
    {
      Name: 'David Slanina',
      Age: 23,
      Skill: 'JavaScript',
    },
    {
      Name: 'David Novák',
      Age: 28,
      Skill: 'HTML',
    },
    {
      Name: 'Josef Slanina',
      Age: 15,
      Skill: 'Cooking',
    },
    {
      Name: 'David LongLong Slanina',
      Age: 35,
      Skill: 'CSS',
    },
    {
      Name: 'Petr Slanina',
      Age: 22,
      Skill: 'JavaScript',
    },
    {
      Name: 'David Slanina',
      Age: 23,
      Skill: 'JavaScript',
    },
  ];

  constructor() {
    console.log(this.tableData);
  }
}
