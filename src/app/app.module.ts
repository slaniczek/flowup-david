import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { SearchComponent } from './components/search/search.component';
import { UserMenuComponent } from './components/user-menu/user-menu.component';
import { DropdownComponent } from './components/dropdown/dropdown.component';
import { HomeComponent } from './views/home/home.component';
import { TileComponent } from './components/tile/tile.component';
import { TitleComponent } from './components/title/title.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { MenuMainComponent } from './components/menu-main/menu-main.component';
import { ButtonComponent } from './components/button/button.component';
import { UserComponent } from './components/user/user.component';
import { TableComponent } from './components/table/table.component';
import { ClickOutsideDirective } from './directives/click-outside.directive';
import { TileListComponent } from './components/tile-list/tile-list.component';
import { DefaultItemComponent } from './components/dropdown/items/default-item/default-item.component';
import { TypographyComponent } from './views/typography/typography.component';
import { NotifyItemComponent } from './components/dropdown/items/notify-item/notify-item.component';
import { TogglerComponent } from './components/toggler/toggler.component';
import { OnlyMobileComponent } from './components/only-mobile/only-mobile.component';
import { MoneyPipe } from './pipes/money.pipe';
import { MessageItemComponent } from './components/dropdown/items/message-item/message-item.component';

@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    SearchComponent,
    UserMenuComponent,
    DropdownComponent,
    HomeComponent,
    TileComponent,
    TitleComponent,
    SidebarComponent,
    MenuMainComponent,
    ButtonComponent,
    UserComponent,
    TableComponent,
    ClickOutsideDirective,
    TileListComponent,
    DefaultItemComponent,
    MessageItemComponent,
    TypographyComponent,
    NotifyItemComponent,
    TogglerComponent,
    OnlyMobileComponent,
    MoneyPipe,
  ],
  imports: [BrowserModule, AppRoutingModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
