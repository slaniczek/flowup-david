import { browser, by, element } from 'protractor';
import { promise as wdpromise } from 'selenium-webdriver';

export class /* CLI generated class name */ {
  navigateTo(): wdpromise.Promise<any> {
    return browser.get('/');
  }

  getParagraphText(): Promise<string> {
    return element(
      by.css(/* css selector (for example 'app-root h1') */),
    ).getText();
  }
}
